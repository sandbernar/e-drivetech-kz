<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FeedbackSent extends Mailable
{
    use Queueable, SerializesModels;

    public $feedback;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($feedback)
    {
        //
        $this->feedback = $feedback;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('robot@e-drivetech.kz')
            ->subject('Обратная связь с сайта')
            ->view('mails.feedback');
    }
}
