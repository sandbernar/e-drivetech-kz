<?php 
namespace App\Http\Controllers;

use TCG\Voyager\Models\Page;

/**
 * 
 */
class PagesController extends Controller
{
	public function show($slug)
	{
		$list = Page::all();
		$entity = Page::where('slug', $slug)->first();
		if (!$entity) {
			die(404);
		}
		return view('pages.page', compact('entity', 'list'));
	}
}