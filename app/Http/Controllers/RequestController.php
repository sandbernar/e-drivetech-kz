<?php 
namespace App\Http\Controllers;

use App\Request as Feedback;
use Illuminate\Support\Facades\Mail;
use App\Mail\FeedbackSent;


use Illuminate\Http\Request;

/**
 * 
 */
class RequestController extends Controller
{
	public function submit(Request $request)
	{
		$record = new Feedback;
		$record->name = $request->input('name');
		$record->phone = $request->input('phone');
		$record->email = $request->input('email');
		$record->phone = $request->input('phone');
		$record->body = $request->input('body');
		$record->company = $request->input('company');
		$record->request_url = $request->input('request_url');

		$record->save();

		Mail::to(['abylay@logistic-systems.kz', 'salamat@e-drivetech.kz'])
			->send(new FeedbackSent($record));

		header('Location: ' . $record->request_url . '?feedback=success');
	}
}