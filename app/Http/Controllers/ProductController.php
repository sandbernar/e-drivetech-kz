<?php 
namespace App\Http\Controllers;

use App\Product;

/**
 * 
 */
class ProductController extends Controller
{
	public function index()
	{
		$list = Product::all();
		return view('products.index', compact('list'));
	}

	public function show($id)
	{
		$list = Product::all();
		$entity = Product::find($id);
		if (!$entity) {
			die(404);
		}
		return view('products.show', compact('entity', 'list'));
	}
}