<?php 
namespace App\Http\Controllers;

use App\News;

/**
 * 
 */
class NewsController extends Controller
{
	public function index()
	{
		$list = News::all();
		return view('news.index', compact('list'));
	}

	public function show($id)
	{
		$list = News::all();
		$entity = News::find($id);
		if (!$entity) {
			die(404);
		}
		return view('news.show', compact('entity', 'list'));
	}
}