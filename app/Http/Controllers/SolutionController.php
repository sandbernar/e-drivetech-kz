<?php 
namespace App\Http\Controllers;

use App\Solution;

/**
 * 
 */
class SolutionController extends Controller
{
	public function index()
	{
		$list = Solution::all();
		return view('solutions.index', compact('list'));
	}

	public function show($id)
	{
		$list = Solution::all();
		$entity = Solution::find($id);
		if (!$entity) {
			die(404);
		}
		return view('solutions.show', compact('entity', 'list'));
	}
}