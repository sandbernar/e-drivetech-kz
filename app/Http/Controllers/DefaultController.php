<?php 
namespace App\Http\Controllers;

use App\Solution;
use App\Product;
use App\Banner;
/**
 * 
 */
class DefaultController extends Controller
{
	public function index()
	{
        $solution = Solution::latest()->take(1)->first();
        $products = Product::latest()->take(5)->get();
        $banners = Banner::latest()->take(4)->get();

		return view('main',
			compact('solution', 'products', 'banners'));
	}
}