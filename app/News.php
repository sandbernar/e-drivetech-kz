<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    public function getCreatedDateAttribute()
	{
		$date = $this->attributes['created_at'];
		$time = strtotime($date);
		return date('d.m.Y');
	}
}
