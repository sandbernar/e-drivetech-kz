<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Product;
use App\Solution;
use TCG\Voyager\Models\Page;
use Illuminate\Support\Facades\View;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $productsInFooter = Product::latest()->take(5)->get();
        $solutionsInFooter = Solution::latest()->take(5)->get();
        $pages = Page::latest()->take(5)->get();
        View::share(compact(
            'productsInFooter',
            'solutionsInFooter',
            'pages'
        ));
    }
}
