
function sendToFriend() 
{ 
	var strPath = '../userdata/sendtofriend.asp?DBID=' + lDatabaseID + '&LNGID=' + lLanguageID;
	var sendToFriendWin = window.open(strPath,'PopUp','width=520,height=500,scrollbars,resizable');
	sendToFriendWin.focus();
}

var ddName = 'Answer';
var ddClickToOpen = 'Click to open the';
var ddClickToClose = 'Click to close the';
if(document.getElementById)
{
	// document.write('<link rel="stylesheet" href="/old_style/style.css" type="text/css" media="screen">');
}
///////////////Open/Close DD for FAQ////////////////

function openAll()
{ 
	var curElement;
	for(i=0; i<document.getElementsByTagName('DD').length; i++)
	{
		curElement = document.getElementsByTagName('DD').item(i);
		curElement.style.display = 'block';
		curElement.previousSibling.title = ddClickToClose + ' ' + ddName;
	}
}

function closeAll()
{
	var curElement;
	for(i=0; i<document.getElementsByTagName('DD').length; i++)
	{
		curElement = document.getElementsByTagName('DD').item(i);
		curElement.style.display = 'none';
		curElement.previousSibling.title = ddClickToOpen + ' ' + ddName;
	}

}

/////////// standard trim function - clean spaces, tabs and new line symbols at the beginning and the end of the string ////////
function trim(str)
{
	var lead = 0, trail = str.length - 1;
	while(str.charCodeAt(lead)  < 33) lead++;
	while(str.charCodeAt(trail)  < 33) trail--;
	return lead > trail ? '' : str.substring(lead, trail + 1);
}

function notValidCharsInValue(fldValue, fldNotValidChars)
{
	var notGoodChars = fldNotValidChars;
	var i = 0;
	for (i =0; i < fldValue.length; i++)
	{
		if (notGoodChars.indexOf(fldValue.charAt(i)) != -1)	return true; 
	}
	return false;
}

function ValidCharsInValue(fldValue, fldValidChars)
{
	var goodChars = fldValidChars;
	var i = 0;	

	for (i =0; i < fldValue.length; i++)
	{
		if (goodChars.indexOf(fldValue.charAt(i)) == -1) return false; 
	}
	return true;
}

////////// Check the Search forms //////////////
function checkSearch(frmObj)
{
	for (i=0; i<frmObj.length; i++)
	{
		if (frmObj[i].type.indexOf('text') == 0)
		{
			frmObj[i].value = trim(frmObj[i].value);
			if (frmObj[i].value.length == 0)
			{
				alert('Please type the keyword to search');
				frmObj[i].focus();
				return false;
				break;
			}
		}
	}
	return true;
}

////////// Check all other forms //////////////
function isValidEmail(strEmail)
{
	if (notValidCharsInValue(strEmail, "!#$%^&*()+=<>?/,\|~`\"[]") || strEmail.indexOf('@') <= 0 || strEmail.indexOf('.') <= 0) 
	{
		alert('Please type a valid e-mail address!');
		return false;
	}
	return true;
}

function isValidPhone(strPhone) 
{
	if (!ValidCharsInValue(strPhone, "0123456789()-+ ") || strPhone.length < 6)
	{
		alert('Please type a valid phone number!');
		return false;
	}
	return true;
}

function isNumber(strNumber) 
{
	if (!ValidCharsInValue(strNumber, "0123456789 "))
	{
		alert('Please type a valid number!');
		return false;
	} 
	return true;
}

function isHumanName(strName) 
{
	if (notValidCharsInValue(strName, "0123456789!@#$%^&*()_+=<>?/.,\|~`\"[]"))
	{
		alert('Please type a valid human name!');
		return false;
	} 
	return true;
}

function isValidCVFileName(fileName) 
{
	var strExt = fileName.substring(fileName.length - 4, fileName.length);
	strExt = strExt.toLowerCase();	
	if (strExt != ".txt" && strExt != ".rtf" && strExt != ".doc" && strExt != ".pdf")
	{
		alert('Please send a file in one of the following formats: \n \".txt\", \".rtf\", \".doc\", \".pdf\"!');
		return false;
	}
	return true;
}


function isValidPassword(curField)
{
	var strValue=curField.value;
	if ( notValidCharsInValue(strValue, "!#@$%^&*()_+=<>?/,\|~`\"[]") || strValue.length < 6 ) return false;
	return true;
}

function isValidUserName(curField)
{
	var strValue=curField.value;
	if ( notValidCharsInValue(strValue, "!#@$%^&*()_+=<>?/,\|~`\"[]") || strValue.length < 6 ) return false;
	return true;
}

function isValidNickName(curField)
{
	var strValue=curField.value;
	if ( notValidCharsInValue(strValue, "!#@$%^&*()_+=<>?/,\|~`\"[]") || strValue.length < 6 ) return false;
	return true;
}


function containValidChars(curField)
{
	var curName = curField.name;
	var curFldType = String(curField.getAttribute('fldType')).toUpperCase();
	
	if (curFldType=='EMAIL')
	{ 
		return isValidEmail(curField.value); 
	}
	else if (curFldType=='PHONE' || curFldType=='FAX' || curFldType=='MOBILEPHONE' || curFldType=='CELLPHONE') 
	{ 
		return isValidPhone(curField.value); 
	}
	else if (curFldType=='NUMERIC' || curFldType=='ZIP')
	{ 
		return isNumber(curField.value); 
	}
	else if (curFldType=='FIRSTNAME' || curFldType=='FULLNAME' || curFldType=='LASTNAME' || curFldType=='MIDDLENAME') 
	{ 
		return isHumanName(curField.value); 
	}
	else if (curFldType=='CVFILE')
	{ 
		return isValidCVFileName(curField.value);
	}
	return true;
}

function showAlert(curField)
{
	if ( curField.verefication_alert!=undefined)
	{
		strAlert = curField.getAttribute('verefication_alert').toString();
		alert(strAlert);
	}
	else
	{
		strAlert = 'Please provide all required information.';
		alert(strAlert);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////  checkForm   ////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
// Checks all mandatory inputs in a form...

function checkForm(frmObj)
{
	var found;
	var i, ii;
	var curField, curName, curType;
	var isFilled, isRequired;	
	var passCounter = 0;
	var passField1;
	var curFldType;
	with(frmObj)
	{
		for (i = 0; i < elements.length; i++)
		{
			curField = elements[i];
			if (curField.disabled || curField.tagName == 'FIELDSET' || curField.tagName == 'OBJECT') continue;
			curField.value = trim(curField.value);
			curName = curField.name;
			curType = curField.type;
			isFilled =  trim(curField.value) != '' ? true : false;
			isRequired = String(curField.getAttribute('IsMandatory')).toUpperCase() == 'TRUE' ? true : false;
			curFldType = String(curField.getAttribute('fldType')).toUpperCase();

			if(curType == 'text' && isFilled && !containValidChars(curField))
			{
					openRelevantTab(curField, frmObj);
					curField.focus();
					curField.select();
					return false;
			}
			else if (elements[curName].length && isRequired &&  (curType == 'radio' || curType == 'checkbox'))
			{
				found = 0;
				for (ii = 0; ii < elements[curName].length; ii++)
				{
					if (elements[curName][ii].checked)
					{
						found = 1;
						break;
					}
				}
				if (found == 0)
				{
					showAlert(curField);
					openRelevantTab(curField, frmObj);
					elements[curName][0].focus();	
					//elements[curName][0].click();
					return false;
				}
			}
			else if (isRequired && curType == 'select-one')
			{
				if (curField.selectedIndex == 0)
				{
					showAlert(curField);
					openRelevantTab(curField, frmObj);
					curField.focus();
					return false;
				}
			}
			else if (isRequired && curType == 'select-multiple')
			{
				if (curField.selectedIndex == -1)
				{
					showAlert(curField);
					openRelevantTab(curField, frmObj);
					curField.focus();
					return false;
				}
			}
			else if (isRequired && (curType == 'text' || curType == 'textarea' || curType == 'password'))
			{
				if (!isFilled)
				{
					showAlert(curField);
					openRelevantTab(curField, frmObj);
					curField.focus();
					return false;
				}
				if ( curType == 'text' && curFldType=='USERNAME' && !isValidUserName(curField))
				{
					alert('Username length have to be more than 6 chars.');
					openRelevantTab(curField, frmObj);
					curField.focus();
					return false;
				}
				if ((curType == 'password' || curFldType=='PASSWORD') && !isValidPassword(curField))
				{
					alert('Password length have to be more than 6 chars.');
					openRelevantTab(curField, frmObj);
					curField.focus();
					return false;
				}
			}
			else if (isRequired && curType == 'hidden')
			{
				if (!isFilled)
				{
					showAlert(curField);
					openRelevantTab(curField, frmObj);
					eval(curField.getAttribute('errorEval'));
					return false;
				}
			}
			else if(curType == 'file' && isFilled && !containValidChars(curField))
			{
					openRelevantTab(curField, frmObj);
					curField.focus();
					curField.select();
					return false;
			}

			if ((curType == 'password' || curFldType=='PASSWORD') && curFldType!='ORIGINALPASSWORD' && passCounter == 0)
			{
				passCounter++;
				passField1 = curField;
			}
			else if ((curType == 'password' || curFldType=='PASSWORD') && curFldType!='ORIGINALPASSWORD' && passCounter == 1)
			{
				if (passField1.value != curField.value)
				{
					alert('The entered passwords do not match.\nPlease re enter them.');
					curField.value = "";
					openRelevantTab(curField, frmObj);
					passField1.focus();
					passField1.select();
					return false;
				}
			}
			
		}
	}
	return true;
}




////////////////////////////////////////////////////////////
// this function toggles tabs and related content divs on same page
////////////////////////////////////////////////////////////

function toggleTab(tabID, objA)
{
	//open related div by ID
	var arrAllDivs = document.getElementsByTagName('DIV');
	for (i=0; i<arrAllDivs.length; i++)
	{
		if (arrAllDivs[i].id.indexOf('tabDiv_') == 0) arrAllDivs[i].style.display = 'none';
	}
	document.getElementById('tabDiv_'+tabID).style.display = 'block';

	//set current tab selected ( <TD class="this"> )
	var objTabTd = objA.parentNode.parentNode.firstChild;
	do
	{
		objTabTd.className="";
		objTabTd = objTabTd.nextSibling; 
	}
	while (null != objTabTd)

	objA.parentNode.className = "this";
}

////////////////////////////////////////////////////////////
// if we need open a tab immediately onload
// place this JS+ASP code on the page:
// addEvent(window, 'load', function(){toggleTabOnLoad(<%=strCurrentTab%>)});
////////////////////////////////////////////////////////////
function toggleTabOnLoad(tabID)
{
	if(document.getElementById('tabA_' + tabID))
	{
		toggleTab(tabID, document.getElementById('tabA_' + tabID))
	}
}

//////////////////////////////////////////////////////////////////////
// if we work with tabs, we must first open the relevant tab
// the function begins work if we have any tab with 'tabDiv_' string in ID
// each div with 'tabDiv_X' string in ID must have related '<a id="tabA_X">' element
//////////////////////////////////////////////////////////////////////
function openRelevantTab(curObj, objForm)
{
	var arrDivs = objForm.getElementsByTagName('DIV');
	var bTabsFound = false;
	var lDivIdNum;
	for (i = 0; i < arrDivs.length; i++ )
	{
		if(arrDivs[i].id.indexOf('tabDiv_') == 0)
		{
			bTabsFound = true;
			break;
		}
	}
	if(bTabsFound)
	{
		while(curObj.tagName != 'FORM' && curObj.style.display != 'none') curObj = curObj.parentNode;
		if(curObj.style.display == 'none' && curObj.id.indexOf('tabDiv_') == 0)
		{
			lDivIdNum = curObj.id.substring(curObj.id.lastIndexOf('_')+1, curObj.id.length);
			toggleTab(lDivIdNum, document.getElementById('tabA_' + lDivIdNum));
		}
	}
}