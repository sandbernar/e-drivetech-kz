var slide_now = 0;
var slide_max = 0;
var tout = null;
var _pause = 1

$(function(){
	
	$('#slide div:first').show();
	slide_max = $('#slide').children().size();
	
	$('#slide').children().bind('mouseover',function(){
		_pause=0;
	});
	
	
	$('#slide').children().bind('mouseout',function(){
		_pause=1;
	});

	$('.btn_anterior').click(function(e){
		e.preventDefault();
		$('#slide div:eq('+slide_now+')').fadeOut();
		slide_now--;
		if(slide_now == -1){
			slide_now = slide_max-1;
		}
		$('#slide div:eq('+slide_now+')').fadeIn();
		
		setBullet(slide_now);
	});
	
	$('.btn_siguiente').click(function(e){
		e.preventDefault();
		go_siguiente();
	});
	
	tout = setInterval("go_siguiente();",5000);
	

	$('#tarjetas a').hover(function(){
		$(this).animate({
			height:291,
			marginTop:'-14px'
		});
	},function(){
		$(this).animate({
			height:277,
			marginTop:'0'
		});
	});
});

function msg(msg,status){
	if(status == 1){
		bg = '#cc0000';
	}else{
		bg = '#339933';
	}
	
	$.blockUI({
		message: msg,
		css: { backgroundColor: bg, color: '#fff', padding: '20px 30px', border: 0},
		timeout: 5000
	});
}

function go_siguiente(){
	if (_pause) {
	$('#slide div:eq('+slide_now+')').fadeOut('slow');
	slide_now++;
	if(slide_now == slide_max){
		slide_now = 0;
	}
	$('#slide div:eq('+slide_now+')').fadeIn('slow');
		
	setBullet(slide_now);
	}
}

function setBullet(id){
	resetTimer();
	$('.bullet').removeClass('activo');
	$('.bullet:eq('+id+')').addClass('activo');
}

function goSlide(id){
	$('#slide div:eq('+slide_now+')').fadeOut('slow');
	slide_now = id;
	$('#slide div:eq('+slide_now+')').fadeIn('slow');
	setBullet(id);
}

function resetTimer(){
	clearInterval(tout);
	tout = setInterval("go_siguiente();",5000);
}






