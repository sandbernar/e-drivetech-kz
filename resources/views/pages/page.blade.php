@extends('layouts.inner', [
	'title' => $entity->title,
	'description' => $entity->meta_description,
	'keywords' => $entity->meta_keywords,
	])

@section('links')

	@foreach ($list as $item)
		<a href="/pages/{{ $item->slug }}">{{ $item->title }}</a>
	@endforeach
	
@endsection('links')

@section('main')

<div id="centercolHide">
	<div class="breadcrumbs">
		<a href='/'>Главная</a>&nbsp;&#187;&nbsp;
		<a href='/pages/o-nas'>О нас</a>&nbsp;&#187;&nbsp;
		{{ $entity->title }}
	</div>

	<h1>{{ $entity->title }}</h1>
	
	<div class="floatright space">
		<a href="{{ Voyager::image($entity->images) }}" target="_blank" class="thickbox">
			<img src="{{ Voyager::image($entity->images) }}" width="300"/>
		</a>
	</div>
	<h3>

	{!! $entity->body !!}

	<div class="clear"></div>

</div>

@endsection('main')