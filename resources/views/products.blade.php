@extends('layouts.app')

@section('content')


<div class="pixels"> 

<div id="leftcol">
	
			<div class="menulevel0">

			<a href="http://www.e-drivetech.com/products/worldfleetlog"  target="_self" title="WorldFleetLog">WorldFleetLog</a>
			
			<a href="http://www.e-drivetech.com/products/F1-FMS"  target="_self" title="F1 FMS">F1 FMS</a>
			
			<a href="http://www.e-drivetech.com/products/RF-Sensors"  target="_self" title="RF Sensors">RF Сенсоры</a>
			
			<a href="http://www.e-drivetech.com/products/Digital-Video-Recorder"  target="_self" title="Mobile DVR and Dashcam Solutions">Мобильный DVR и решения видеорегистраторы</a>
			
			<a href="http://www.e-drivetech.com/products/SmartSight-S1-Dashcam"  target="_self" title="SmartSight S1 Dashcam">Ыидеорегистатор SmartSight S1</a>
			
			</div>
<script type="text/javascript"> 
$(document).ready(function() {
   $("#ContactSendBtn").click(function() {
		var _name = $("#hfld_Name").val()
		var _phone = $("#hfld_Phone").val()
		var _email = $("#hfld_Email").val()
		var _body = $("#hfld_Comments").val()
		var _company = $("#hfld_Company").val()
		
		var _Country = $("#hfld_Country").val()
		
		var _captch = $("#hfld_Captcha").val()
		var _captchError = $("#hfld_CaptchaError").val()
		var _successMsg	=$("#hfld_Success").val()
		if (_email.length == 0) {
			alert("You entered the wrong digits. Please try again.");
			return false;
		}
		$.post("/EDT/Templates/contact.asp", { name: _name,company: _company, email:_email, country:_Country,body:_body,captch:_captch,link:pageURL,Slink:strPageName  },
			function(data){
				if (data == "0") {
					$("#ContactResponseMsg").html("<span>"+ _captchError +"</span>");
				}else{
				$("#ContactResponseMsg").addClass("contact")
				$("#ContactForm").fadeOut("slow");
				$("#ContactSendBtn").fadeOut("slow");
				$("#ContactResponseMsg").html(_successMsg);
					_gaq.push(['_trackEvent', 'Fast_Contact', 'Thank_You', '', '']);
					setTimeout(function(){
				$("#hfld_Name").val("");
				$("#hfld_Phone").val("");
				$("#hfld_Email").val("");
				$("#hfld_Comments").val("");
				$("#hfld_Captcha").val("");
				$("#hfld_Company").val(""); 
					
					$("#hfld_Country").val(""); 
										  
				$("#ContactResponseMsg").removeClass("contact");
				$("#ContactForm").fadeIn("slow");
				$("#ContactSendBtn").fadeIn("slow");
				$("#ContactResponseMsg").html();
				}, 4000);
				}
		});
   });
 });
</script>
<h3 class="contacttitle">Задавайте вопросы</h3>
<div class="frame">
	<div id="ContactForm" class="contact">
		<input id="hfld_Name" onfocus="if (this.value==this.defaultValue) this.value='';" type="text" value="Имя" />
		<input id="hfld_Email" onfocus="if (this.value==this.defaultValue) this.value='';" value="E-mail" type="text" />
		<input id="hfld_Company" onfocus="if (this.value==this.defaultValue) this.value='';" value="Компания" type="text" />
		
		<textarea id="hfld_Comments" onfocus="if (this.value==this.defaultValue) this.value='';" rows="2" cols="20">Сообщение</textarea>
		<span class="long">Введите числа</span>
		<img id="ImgCaptcha" src="/EDT/Templates/forms/captcha.asp" alt="Введите цифры с картинки" />
		<input type="text" id="hfld_Captcha" />
		<input class="inputcolor3" id="ContactSendBtn" type="submit" name="Отправить"/>
		<!-- <button>Отправить</button> -->
	</div>
</div>
<div class="contactend">
	<input type="hidden" id="hfld_CaptchaError" value="Wrong numbers" />
	<input type="hidden" id="hfld_Success" value="Your message was sent successfully" />
</div>
<div id="ContactResponseMsg" class="contactend"></div>


&nbsp;
</div>


<div id="centercol">
<div class="breadcrumbs"><a href='/EDT/index.asp'>Главная</a>&nbsp;&#187;&nbsp;Продукты EDT</div>
<h1>Продукты EDT</h1>
<h2>Продукты и решения EDT обеспечивают превосходную производительность FMS ваших клиентов, подкрепленную непревзойденной поддержкой клиентов.</h2>


<div class="imglist">

	
	<a href="http://www.e-drivetech.com/products/worldfleetlog">
		<h4>WorldFleetLog</h4>
	</a>
	WorldFleetLog - это облачное приложение EDT для управления автопарком, предназначенное для отображения и управления данными, собранными из продуктов EDT для управления автопарком и топливом. С WorldFleetLog у менеджеров автопарков в распоряжении есть разнообразная информация для отслеживания транспортных средств в режиме реального времени, управления активами и мониторинга водителя.
	
	<br />	
	<a href="http://www.e-drivetech.com/products/worldfleetlog">
		Далее...
	</a>
</div>




<div class="imglist">

	
	<a href="http://www.e-drivetech.com/products/F1-FMS">
		<h4>F1 FMS</h4>
	</a>
	FMS F1 предназначена для приложений государственного, военного, коммерческого и частного секторов, которые требуют высокой производительности.
	<a href="http://www.e-drivetech.com/products/F1-FMS">
			<br />	Далее...
			</a>
</div>




<div class="imglist">

	
	<a href="http://www.e-drivetech.com/products/RF-Sensors">
		<h4>RF Сенсоры</h4>
	</a>
	Радиочастотный приемник и датчики EDT SmartSense полностью интегрированы с EDT F1 и SmartLog 5 IVD для отображения данных датчика в режиме реального времени. Облачное приложение EDT WorldFleetLog предоставляет подробные и сводные отчеты об активности датчиков.
	<a href="http://www.e-drivetech.com/products/RF-Sensors">
			<br />	Далее...
			</a>
</div>


	<div class="clear"></div>



<div class="imglist">

	
	<a href="http://www.e-drivetech.com/products/Digital-Video-Recorder">
		<h4>Решения для мобильных видеорегистраторов и видеорегистраторов</h4>
	</a>
	Линейка продуктов EDT для видеорегистраторов и видеорегистраторов EDT является последним дополнением к набору решений EDT для управления автопарком, топливом и активами. Решения EDT DVR полностью интегрированы с WorldFleetLog. Это позволяет управляющим автопарком и активами иметь прямой доступ к видео в реальном времени с каждого транспортного средства или ресурса, перечисленных в WorldFleetLog.
	<a href="http://www.e-drivetech.com/products/Digital-Video-Recorder">
			<br />	Далее...
			</a>
</div>




<div class="imglist">

	
	<a href="http://www.e-drivetech.com/products/SmartSight-S1-Dashcam">
		<h4>Регистратор SmartSight S1</h4>
	</a>
	80% дорожно-транспортных происшествий связаны с отвлечением водителя
	Уровень аварийности торгового флота достигает 20%
	Отвлеченное вождение можно предотвратить
	<a href="http://www.e-drivetech.com/products/SmartSight-S1-Dashcam">
			<br />	Далее...
			</a>
</div>




	<br /><div class="doclinkimgtitle">Это ссылка</div>
	<div class="doclinkimg">
	<img src="/EDT/images/link.gif" alt="WorldFleetLog" align="middle" />
	<a href="/products/worldfleetlog" target="_self" title="WorldFleetLog" >WorldFleetLog</a>
	</div>

</div>

<div id="rightcol">
	
	&nbsp;
</div>


</div>
</div>

@endsection
