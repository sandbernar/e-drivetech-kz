@extends('layouts.inner')

@section('links')
	
	@foreach ($list as $item)
		<a href="/product/{{ $item->id }}">{{ $item->name }}</a>
	@endforeach

@endsection('links')

@section('main')

<div id="centercol">
	<div class="breadcrumbs"><a href='/EDT/index.asp'>Главная</a>&nbsp;&#187;&nbsp;Продукты EDT</div>


	<h1>Продукты EDT</h1>
	<h2>Продукты и решения EDT обеспечивают превосходную производительность FMS ваших клиентов, подкрепленную непревзойденной поддержкой клиентов.</h2>

	@foreach ($list as $item)

		<div class="imglist">

			<a href="http://www.e-drivetech.com/products/worldfleetlog">
				<h4>{{ $item->name }}</h4>
			</a>
			{{ $item->brief }}
			
			<br />	
			<a href="/product/{{ $item->id }}">
				Далее...
			</a>
		</div>
		
	@endforeach

</div>
	
@endsection('main')