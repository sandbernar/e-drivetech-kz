<table>
	<tr>
		<td>Имя</td>
		<td>{{ $feedback->name }}</td>
	</tr>
	@if ($feedback->email)
		<tr>
			<td>E-mail</td>
			<td>{{ $feedback->email }}</td>
		</tr>
	@endif
	@if ($feedback->phone)
		<tr>
			<td>Телефон</td>
			<td>{{ $feedback->phone }}</td>
		</tr>
	@endif
	@if ($feedback->company)
		<tr>
			<td>Компания</td>
			<td>{{ $feedback->company }}</td>
		</tr>
	@endif
	@if ($feedback->body)
		<tr>
			<td>Сообщение</td>
			<td>{{ $feedback->body }}</td>
		</tr>
	@endif
	@if ($feedback->request_url)
		<tr>
			<td>Страница обращения</td>
			<td>{{ $feedback->request_url }}</td>
		</tr>
	@endif
</table>