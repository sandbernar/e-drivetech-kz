@extends('layouts.inner')

@section('links')

	@foreach ($list as $item)
		<a href="/news/{{ $item->id }}">{{ $item->name }}</a>
	@endforeach
	
@endsection('links')

@section('main')

<div id="centercolHide">
	<div class="breadcrumbs">
		<a href='/'>Главная</a>&nbsp;&#187;&nbsp;
		<a href='/news'>Новости EDT</a>&nbsp;&#187;&nbsp;
		{{ $entity->name }}
	</div>

	<h1>{{ $entity->name }}</h1>
	
	<div class="floatright space">
		<a href="{{ Voyager::image($entity->images) }}" target="_blank" class="thickbox">
			<img src="{{ Voyager::image($entity->images) }}" width="300"/>
		</a>
	</div>
	<h3>

	{!! $entity->body !!}

	<div class="clear"></div>

</div>

@endsection('main')