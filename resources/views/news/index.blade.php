@extends('layouts.inner')

@section('links')
	
	@foreach ($list as $item)
		<a href="/news/{{ $item->id }}">{{ $item->name }}</a>
	@endforeach

@endsection('links')

@section('main')

<div id="centercol">
	<div class="breadcrumbs"><a href='/EDT/index.asp'>Главная</a>&nbsp;&#187;&nbsp;Новости EDT</div>


	@foreach ($list as $item)

		<div class="news">

			<a href="/news/{{ $item->id }}">
				<h4>{{ $item->name }}</h4>
			</a>
			<span>{{ $item->created_date }}</span>
			{{ $item->brief }}
			
			<br />	
			<a href="/news/{{ $item->id }}">
				Далее...
			</a>
		</div>
		
	@endforeach

</div>
	
@endsection('main')