<!DOCTYPE html>
<html>
<head>

<?php 
	$separator = isset($title) ? ' | ' : '';
?>
<title>{{ ($title ?? '') . $separator . setting('site.meta_title') }}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="{{ $description ?? setting('site.meta_description') }}" />
<meta name="keywords" content="{{ $keywords ?? '' }}" />


<link rel="shortcut icon" href="/old_static/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" href="/old_static/style.css" media="screen,projection,print" />

<link rel="stylesheet" type="text/css" href="/old_static/thickbox.css" />
<link rel="stylesheet" type="text/css" href="/old_static/print.css" media="print" />
<script type="text/javascript" src="/old_static/js.js"></script>
<script type="text/javascript" src="/old_static/jquery-1.3.min.js"></script>
<script type="text/javascript" src="/old_static/jquery.Thickbox.js"></script>
<script type="text/javascript" src="/old_static/slider.js"></script>
</head>
<body>

	<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(56624890, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/56624890" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154423628-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-154423628-1');
</script>


<div id="theboss" class="page"
>

	<a href="/" title="E-Drivetech Eurasia">
		<img src="/old_static/logo.jpg" class="logo" alt="E-Drivetech Eurasia" title="E-Drivetech Eurasia" />
	</a>
	
	
	<span class="slogan">
		<strong>{{ setting('site.title') }}</strong><br />
		{{ setting('site.description') }}
	</span>
	
	<div id="menu">
		
		<div ><a href="/products">Продукты EDT</a></div>
		<img class="marker" src="/old_static/marker.gif" alt="" />
		<div ><a href="/solutions">Решения</a></div>
		<img class="marker" src="/old_static/marker.gif" alt="" />
		<div ><a href="/news">Новости и события</a></div>
		<img class="marker" src="/old_static/marker.gif" alt="" />
		<div ><a href="/pages/podderjka">Поддержка</a></div>
		<img class="marker" src="/old_static/marker.gif" alt="" />
		<div ><a href="/pages/klienty">Наши клиенты</a></div>
		<img class="marker" src="/old_static/marker.gif" alt="" />
		<div ><a href="/pages/o-nas">О нас</a></div>
		<img class="marker" src="/old_static/marker.gif" alt="" />
		<div ><a href="/pages/kontakty">Контакты</a></div>
		<img class="marker" src="/old_static/marker.gif" alt="" />
	</div>
				

    @yield('content')
 
</div>

<div class="footertextin">
	<div class="quickbox">
		<h4>Продукты EDT</h4>
		@foreach ($productsInFooter as $product)
			<a href="/product/{{ $product->id }}">{{ $product->name }}</a><br />
		@endforeach
	</div>

	<div class="quickbox">
		<h4>Решения EDT</h4>
		@foreach ($solutionsInFooter as $solution)
			<a href="/solution/{{ $solution->id }}">{{ $solution->name }}</a><br />
		@endforeach
	</div>
	<div class="quickbox">
		<h4>О EDT</h4>
		@foreach ($pages as $page)
			<a href="/pages/{{ $page->slug }}">{{ $page->title }}</a><br />
		@endforeach
	</div>
</div>

<script src="https://unpkg.com/imask"></script>
<script>
	var element = document.getElementById('phoneMasked');
	var maskOptions = {
	  mask: '+{7}(000)000-00-00'
	};
	var mask = IMask(element, maskOptions);
</script>
			
</body>
</html>