@extends('layouts.app', [
	'title' => $title ?? null,
	'description' => $description ?? null,
	'keywords' => $keywords ?? null,
	])

@section('content')


<div class="pixels"> 

<div id="leftcol">
	
	<div class="menulevel0">

		@yield('links')
	
	</div>
<h3 class="contacttitle">Задавайте вопросы</h3>
<form class="frame" action="/request" method="POST">

	@if (isset($_GET['feedback']) && $_GET['feedback'] == 'success')
		<h4 style="text-align: center; color: red">Ваше сообщение отправлено!</h4>
	@endif
	<div id="ContactForm" class="contact">
		<input onfocus="if (this.value==this.defaultValue) this.value='';" type="text" value="Имя" name="name" required>
		<input onfocus="if (this.value==this.defaultValue) this.value='';" value="E-mail" type="email" name="email">
		<input type="tel" name="phone" id="phoneMasked" placeholder="+7(___)___-__-__">
		<input id="hfld_Company" onfocus="if (this.value==this.defaultValue) this.value='';" value="Компания" type="text" name="company">
		
		<textarea id="hfld_Comments" onfocus="if (this.value==this.defaultValue) this.value='';" rows="2" cols="20" name="body">Сообщение</textarea>
		<button>Отправить</button>
		<!-- <button>Отправить</button> -->
		@csrf
		<input type="hidden" name="request_url" value="{{ url()->current() }}">
	</div>
</form>
<div class="contactend">
</div>


&nbsp;
</div>


@yield('main')


<div id="rightcol">
	
	&nbsp;
</div>


</div>
</div>

@endsection
