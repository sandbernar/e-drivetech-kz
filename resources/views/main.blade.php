@extends('layouts.app')

@section('content')

	<style>
		#slide > div {
			background-position: center center;
			background-repeat: no-repeat;
		}
	</style>
	<div class="sun">
	<div class="co_left">
	    
	    <div id="cont">
			
			<div id="slide">
				@foreach ($banners as $item)
					
					<div style="background-image: url({{ Voyager::image($item->image) }});">
						
						<span class="trans">
							<a href="{{ $item->link }}" >
								<img src="/old_static/arrow.png" alt /></a>
							<p>{{ $item->brief }}</p>
						</span>
					
					</div>
				@endforeach


			</div>
			
			<div class="bullets">
				@php
					$i = 0;
				@endphp

				@foreach ($banners as $item)
					<a href="javascript:goSlide({{ $i }})" class="bullet activo tab_{{ ($i+1) }}">
						<h2>{{ $item->name }}</h2>
					</a>
					@php
						$i++;
					@endphp
				@endforeach
				
			</div>
			
		</div>
		

		<div class="textcontent">
			<h1>{{ setting('main.infoblock_title') }}</h1>
			{!! setting('main.infoblock_body') !!}
		</div>
	</div>

	<div class="co_right">

		<div class="orangebox">
			<h3>Продукты EDT</h3>
			<div>
				@foreach ($products as $element)
					<a href="/product/{{ $element->id }}">{{ $element->name }}</a>
				@endforeach

			</div> 
			<a href="/products" class="all">Все продукты</a>
		</div>
		
		<div class="orangeboxright">
			<h3>Решение EDT</h3>
			<div>

				@if (isset($solution->image))
					<a href="/solution/{{ $solution->id }}">
						<img src="{{ Voyager::image($solution->image) }}">
					</a>
				@endif
				

			
			<a href="/solution/{{ $solution->id }}">
		
				<span class="title">{!! $solution->brief !!}</span>
			</a>
			

			</div>
			<a href="/products" class="all">Все решения</a>
		</div>
		
		
			<div class="feature" >
				
				<a href="/news" >
					
			<img src="/old_static/7643653849_Small.png" alt="Login Page" title="Login Page" />
		
				</a>
				
				<a href="/news" >Объявление о выпуске WFL 3.20</a>
				
		<div class="clear"></div>
	<p>Предлагаем ознакомиться с последним обновлением WorldFleetLog. <a href="/news">Последний релиз</a> включает в себя множество новых ориентированных на клиента функций</p>
			</div>


			<div class="feature" style='margin-right:0px;'>
				
				<a href="http://www.e-drivetech.com/products/wflmobile"  target='_blank' >
					
					<img src="/old_static/1795876927_Small.png" alt="WFLMobile" title="WFLMobile" />
		
				</a>
				
				<a href="http://www.e-drivetech.com/products/wflmobile"  target='_blank' >Приложение World Fleet</a>
				
		<div class="clear"></div>
	<p>Приложение EDT World Fleet обеспечивает мобильный доступ на ходу! Наш последний выпуск включает в себя новый интерфейс с расширенными функциями и отличным пользовательским интерфейсом. <a href="http://www.e-drivetech.com/products/wflmobile" target="_blank">Теперь <strong>поддерживает Push-уведомления</strong></a>.</p>
			</div>


			<div class="feature" >
				
				<a href="https://www.worldfleetlog.com/wfl/" >WorldFleetLog</a>
				
			<div class="clear"></div>
				<p>Авторизоваться в <a href="https://www.worldfleetlog.com/wfl/">WorldFleetLog.</a></p>
			</div>


			<div class="feature" style='margin-right:0px;'>
				
				<a href="/pages/podderjka" >
					
			<img src="/old_static/6535783140_Small.jpg" alt="EDT Customer Support" title="EDT Customer Support" />
		
				</a>
				
				<a href="/pages/podderjka" >Служба поддержки</a>
				
		<div class="clear"></div>
		<p>Свяжитесь с нами для технических запросов и запасных частей. Проверьте наши часто задаваемые вопросы, скачать калькулятор экономии затрат!</p>
		</div>


	</div>

	</div>
@endsection