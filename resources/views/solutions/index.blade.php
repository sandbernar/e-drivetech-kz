@extends('layouts.inner')

@section('links')
	
	@foreach ($list as $item)
		<a href="/solution/{{ $item->id }}">{{ $item->name }}</a>
	@endforeach

@endsection('links')

@section('main')

<div id="centercol">
	<div class="breadcrumbs"><a href='/EDT/index.asp'>Главная</a>&nbsp;&#187;&nbsp;Решения EDT</div>


	<h1>Решения для автомобильной телематики, управления топливом и отслеживания активов</h1>

	@foreach ($list as $item)

		<div class="imglist">

			<a href="http://www.e-drivetech.com/solutions/worldfleetlog">
				<h4>{{ $item->name }}</h4>
			</a>
			{{ $item->brief }}
			
			<br />	
			<a href="/solution/{{ $item->id }}">
				Далее...
			</a>
		</div>
		
	@endforeach

</div>
	
@endsection('main')