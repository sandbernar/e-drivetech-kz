<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DefaultController@index')->name('index');
Route::get('/products', 'ProductController@index')->name('product.index');
Route::get('/product/{id}', 'ProductController@show')->name('product.show');

Route::get('/solutions', 'SolutionController@index')->name('solution.index');
Route::get('/solution/{id}', 'SolutionController@show')->name('solution.show');

Route::get('/news', 'NewsController@index')->name('news.index');
Route::get('/news/{id}', 'NewsController@show')->name('news.show');

Route::get('/news', 'NewsController@index')->name('news.index');

Route::get('/pages/{slug}', 'PagesController@show')->name('pages.show');


Route::post('/request', 'RequestController@submit')->name('request.post');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
